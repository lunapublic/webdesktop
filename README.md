# Webdesktop

Lightweight web based desktop using novnc and x11vnc.


Supports PUID and PGID in a similar way as it is done in the linuxserver.io containers: https://docs.linuxserver.io/general/understanding-puid-and-pgid. Can sudo inside unless PGID is defined


Example use:

```
docker run -d --rm -p 127.0.0.1:6080:6080 gitlab-registry.cern.ch/lunapublic/webdesktop/ubuntu:latest
```

Connecting pulseaudio (only on linux hosts running pulseaudio):

```
docker run -d --rm -v /run/user/$UID/pulse:/run/user/1000/pulse -e PGID=65537 -p 127.0.0.1:6080:6080 gitlab-registry.cern.ch/lunapublic/webdesktop/ubuntu:latest

```


Connecting pulseaudio (only on linux hosts running pulseaudio) and configure firefox for downloading files in specific directory:


```
docker run --log-driver=journald -d --rm -v $PWD/firefox.js:/usr/lib/firefox/defaults/pref/mine.js -v $PWD/downloads:/downloads -v /run/user/$UID/pulse:/run/user/1000/pulse -e PUID=1000 -e PGID=65537 -p 127.0.0.1:6080:6080  gitlab-registry.cern.ch/lunapublic/webdesktop/ubuntu:latest
```



Then point your browser to http://127.0.0.1:6080
