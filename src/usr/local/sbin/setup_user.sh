#!/bin/bash
set -e

if [ $UID = 0 ];then
    export PUID=${PUID:-1000}
    export PGID=${PGID:-1000}
    export PUSER=${PUSER:-appuser}

    if [ -n "$PGID" ]; then
        chown -R :$PGID /home/"$PUSER"
        groupmod -g "$PGID" "$PUSER"
        echo Renamed $PUSER as gid=$PGID
    fi
    if [ -n "$PUID" ]; then
        chown -R $PUID /home/"$PUSER"
        usermod -u "$PUID" "$PUSER"
        echo Renamed $PUSER as uid=$PUID
    fi
fi
