#!/command/with-contenv bash
if [ $UID = 0 ] ; then
    s6-applyuidgid -u "$PUID" -g "$PGID" bash -c 'tty || sleep infinity && /bin/bash'
else
    tty || sleep infinity && /bin/bash
fi

