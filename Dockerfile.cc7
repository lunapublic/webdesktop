FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
ARG S6_OVERLAY_VERSION=3.1.0.1

MAINTAINER Jose Carlos Luna <Jose.Carlos.Luna@cern.ch>


RUN yum update -y  && \
    yum install -y curl xz git procps && \
    yum clean all

ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-x86_64.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz
ENTRYPOINT ["/init"]

RUN yum update -y  && \
    yum install -y  xorg-x11-server-Xvfb x11vnc xterm icewm python3 python3-numpy && \
    yum clean all

ARG NOVNC_VERSION="v1.3.0"
ARG WEBSOCKSIFY_VERSION="v0.10.0"

RUN git config --global advice.detachedHead false && \
    git clone -b $NOVNC_VERSION https://github.com/novnc/noVNC /opt/noVNC && \
    git clone -b $WEBSOCKSIFY_VERSION https://github.com/novnc/websockify /opt/noVNC/utils/websockify && \
    mkdir -p /tmp/.X11-unix/ && chmod 1777 /tmp/.X11-unix/
COPY src/index.html /opt/noVNC
EXPOSE 6080


RUN yum update -y  && \
    yum install -y xterm sudo && \
    yum clean all

COPY src/etc/ /etc/

EXPOSE 6080
ENV PUID=1000
ENV PGID=1000
RUN groupadd --gid $PGID appuser
RUN useradd -d /home/appuser -c appuser --uid $PUID --gid $PGID appuser

#Can sudo
RUN usermod -a -G wheel appuser
RUN echo '%wheel ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers
COPY src/usr/local/sbin/  /usr/local/sbin/
RUN chmod 555 /usr/local/sbin/*

COPY src/xinitrc /home/appuser/.xinitrc
RUN chmod 555 /home/appuser/.xinitrc
CMD /usr/local/sbin/shell.sh
