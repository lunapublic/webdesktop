// -v $PWD/thisfile:/usr/lib/firefox/defaults/pref/mine.js
pref("browser.download.dir","/downloads");
pref("browser.download.downloadDir","/downloads");
pref("browser.download.defaultFolder","/downloads");
pref("pdfjs.disabled", true);
pref("browser.download.useDownloadDir", true);
pref("browser.download.folderList", 2, locked);
